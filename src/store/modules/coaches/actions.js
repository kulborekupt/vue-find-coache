export default {
  async registerCoach(context, data) {
    // const id = Math.random().toString(16).slice(2)
    const id = context.rootGetters.getGenerateId
    const coachData = {
      id: id,
      firstName: data.first,
      lastName: data.last,
      description: data.desc,
      hourlyRate: data.rate,
      areas: data.areas
    }
   
    const token = context.rootGetters.token
    console.log(coachData)
    console.log(token)
    const response = await fetch(
      `https://vue-app-353c5-default-rtdb.asia-southeast1.firebasedatabase.app/coaches/${id}.json?auth=`+token,
      {
        method: 'PUT',
        body: JSON.stringify(coachData)
      }
    )
    // const responseData = await response.json()
    if (!response.ok) {
      //Error
    }

    context.commit('registerCoach', {
      ...coachData,
      id: id
    })
  },
  async loadCoaches(context,payload) {

    if (!payload.forceRefresh && !context.getters.shouldUpdate) {
      return;
    }
    const token = context.rootGetters.token
    console.log('token')
    console.log(token)
    const response = await fetch(`https://vue-app-353c5-default-rtdb.asia-southeast1.firebasedatabase.app/coaches.json?auth=`+token);
    const responseData = await response.json();
    if (!response.ok) {
      const error = new Error(responseData.message || "Failed to fetch");
      throw error;
    }

    const coaches = [];
    for (const key in responseData) {
      const coach = {
        id: key,
        firstName: responseData[key].firstName,
        lastName: responseData[key].lastName,
        description: responseData[key].description,
        hourlyRate: responseData[key].hourlyRate,
        areas: responseData[key].areas,
      };

      coaches.push(coach);
    }
    context.commit('setCoaches', coaches);
    context.commit('setFetchTimeStamp');

  }
}
