import mutations from './mutations.js'
import actions from './actions.js'
import getters from './getters.js'

export default {
  namespaced: true,
  state() {
    return {
      lastFetch: null,
      userIsCoach: false,
      coaches: [
        {
          id: 'c1',
          firstName: 'Prasert',
          lastName: 'Kulborekupt',
          areas: ['frontend', 'backend', 'fullstack'],
          description: 'description',
          hourlyRate: 30
        },
        {
          id: 'c2',
          firstName: 'test2',
          lastName: 'test2',
          areas: ['backend', 'frontend'],
          description: 'description2',
          hourlyRate: 40
        },
        // {
        //   id: 'c3',
        //   firstName: 'test3333',
        //   lastName: 'test333333',
        //   areas: ['backend', 'frontend'],
        //   description: 'description2',
        //   hourlyRate: 40
        // }
      ]
    }
  },
  mutations,
  actions,
  getters
}
