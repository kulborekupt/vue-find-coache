let timer;
export default {
    async login(context, payload) {
        return context.dispatch('auth', {
            ...payload,
            mode: 'login'
        })
    },

    async signup(context, payload) {
        return context.dispatch('auth', {
            ...payload,
            mode: 'signup'
        })
    },

    async auth(context, payload) {
        let url = `https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyA_P3EjSqUT0xGZ49nzbLMs9_RTjaqVPg4`; // SIGNUP
        if (payload.mode === 'login') {
            url = `https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyA_P3EjSqUT0xGZ49nzbLMs9_RTjaqVPg4`;
        }

        const response = await fetch(url, {
            method: 'POST',
            body: JSON.stringify({
                email: payload.email,
                password: payload.password,
                returnSecureToken: true
            })
        })

        const responseData = await response.json()
        if (!response.ok) {
            //Error
            const error = new Error(responseData.message)
            console.log(error)
            throw error;
        }

        const exporeIn = 5000;
        const expireDate = new Date().getTime() + exporeIn ;//incress expire date

        localStorage.setItem('token', responseData.idToken)
        localStorage.setItem('userId', responseData.localId)
        localStorage.setItem('tokenExpire', expireDate)

        timer = setTimeout(function () {
            context.dispatch('autoLogout')
        }, exporeIn)

        context.commit('setUser', {
            token: responseData.idToken,
            userId: responseData.localId,
        })
    },

    tryLogin(context) {
        const token = localStorage.getItem('token');
        const userId = localStorage.getItem('userId');
        const tokenExpire = localStorage.getItem('tokenExpire')
        const expiresIn = +tokenExpire - new Date().getTime();

        if (expiresIn < 0) {
            return;
        }

        setTimeout(function(){
            context.dispatch('autoLogout')
        },expiresIn);

        if (token && userId) {
            context.commit('setUser', {
                token: token,
                userId: userId,
                tokenExpiration: null
            });
        }
    },

    logout(context) {
        localStorage.removeItem('token');
        localStorage.removeItem('userId');
        localStorage.removeItem('tokenExpire');

        clearTimeout(timer);

        context.commit('setUser', {
            token: null,
            userId: null,
            tokenExpiration: null
        });
    },
    autoLogout(context){
        context.dispatch('logout');
        context.commit('setAutoLogout')
    }
}