import requests from ".";

export default {
  async contactCoach({ commit }, payload) {
    const newRequest = {
      id: Math.random().toString(16).slice(2),
      coachId: payload.coachId,
      userEmail: payload.email,
      message: payload.message
    }
    const response = await fetch(`https://vue-app-353c5-default-rtdb.asia-southeast1.firebasedatabase.app/requests/${payload.coachId}.json`, {
      method: 'POST',
      body: JSON.stringify(newRequest)
    });
    const responseData = await response.json();

    if (!response.ok) {
      const error = new Error(responseData.message || 'error to send request.');
      throw error;
    }
    newRequest.id = responseData.name
    newRequest.coachId = payload.coachId

    commit('addRequest', newRequest)
    // this.$router.replace('coaches');
  },
  async fetchRequests(context) {
    const coachId = context.rootGetters.userId
    const token = context.rootGetters.token;
    const response = await fetch(`https://vue-app-353c5-default-rtdb.asia-southeast1.firebasedatabase.app/requests/${coachId}.json?auth=`+token, {
      method: 'GET'
    });
    const responseData = await response.json();
    if (!response.ok) {
      const error = new Error(responseData.message || 'Erorr to get requests')
      throw error
    }

    const requests = [];

    for (const key in responseData) {
      const request = {
        id: key,
        coachId: coachId,
        userEmail: responseData[key].userEmail,
        message: responseData[key].message
      };
      requests.push(request)
    }
    context.commit('setRequests', requests);
  }

}
