export default {
  getRequests(state,getters,rootState,rootGetters) {
    const coachId = rootGetters.userId;
    console.log(coachId,state.requests)
    return state.requests.filter(req => req.coachId === coachId);
  },
  hasRequests(state,getter) {
    return state.requests && state.requests.length > 0
  }
}
