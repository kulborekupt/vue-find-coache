import { createRouter, createWebHistory } from 'vue-router'

import CoacheList from './components/coaches/CoacheList.vue'
import CoacheDetail from './components/coaches/CoacheDetail.vue'
import CoacheRegisteraion from './components/coaches/CoacheRegistration.vue'
import RequestReceived from './components/requests/RequestReceived.vue'
import ContactCoach from './components/requests/ContactCoache.vue'
import NotFound from './components/NotFound.vue'
import UserAuth from './components/auth/UserAuth.vue';
import store from './store/index.js';

const router = createRouter({
  history: createWebHistory(),
  routes: [
    { path: '/', redirect: '/coaches' },
    { path: '/coaches', component: CoacheList },
    {
      path: '/coaches/:id',
      component: CoacheDetail,
      props: true,
      children: [{ path: 'contact', component: ContactCoach }]
    },
    // { path: '/coache/:id/contact', component: ContactCoach },
    { path: '/register', component: CoacheRegisteraion, meta: { requiresAuth: true } },
    { path: '/request', component: RequestReceived, meta: { requiresAuth: true } },
    { path: '/auth', component: UserAuth, meta: { requiresUnauth: true } },
    { path: '/:notFound(.*)', component: NotFound }
  ]
})

router.beforeEach((to, from, next) => {
  if (to.meta.requiresAuth && !store.getters.isAuthenticated) {
    next('/auth');
  } else if (to.meta.requiresUnauth && store.getters.isAuthenticated) {
    next('/coaches')
  } else {
    next();
  }
});

export default router
